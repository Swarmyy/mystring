#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace std;

class mystring
{
private:
	int spe;                                                     //spe �tant les lettres "sp�ciales"(non char de l alphabet)
	int*stat;                                                    //occurence des lettres de l'alphabet
	void maj();                                                  //mise a jour
	void zstat();                                                //met a zero stat
public:
	char*tab;
	int n;
	mystring();
	mystring(const char*);
	mystring(int, char);
	mystring(const mystring&);
	~mystring();
	void affiche();
	//void affiche(ostream&) const;
	void del(char);
	void ddouble(char);
	char* concat(char*, char*);
	void toCaps();
	mystring& operator=(const mystring &);
	mystring operator+(const mystring &);
	mystring operator-(char);
	bool operator==(const mystring &);
	bool operator!=(const mystring &);
	friend ostream& operator<<(ostream&, const mystring &);
};
mystring::mystring() {
	tab = NULL;
	n = spe = 0;
	stat = new int[26];
	zstat();
	maj();
}
mystring::mystring(const char*str) {
	n = strlen(str);
	spe = 0;
	tab = new char[n + 1];
	stat = new int[26];
	strcpy(tab, str);
	zstat();
	maj();
}
mystring::mystring(int sz, char c) {
	n = sz;
	spe = 0;
	tab = new char[n + 1];
	stat = new int[26];
	for (int i = 0; i < n; i++) {
		tab[i] = c;
	}
	tab[n] = '\0';
	zstat();
	maj();
}
mystring::mystring(const mystring &mstr) {
	tab = new char[n = mstr.n];
	stat = new int[26];
	for (int i = 0; i < n; i++) {
		tab[i] = mstr.tab[i];
	}
	zstat();
	maj();
}
mystring::~mystring() {
	delete tab;
	delete stat;
}
void mystring::affiche() {
	if (tab != NULL) {
		for (int i = 0; i < n; i++)
		{
			printf("%c", tab[i]);
		}
	}
}
void mystring::del(char c) {
	int i, j;
	int rm = 0;
	char *dest, *src, *origin;
	for (origin = tab; *origin != '\0'; origin++) {
		if (*origin == c) {
			rm++;
		}
	}
	dest = (char*)malloc((strlen(tab) + 1 - rm) * sizeof(char));
	src = dest;
	for (origin = tab; *origin != '\0'; origin++) {
		if (*origin != c) {
			*src = *origin;
			src++;
		}
	}
	*src = '\0';
	free(tab);
	tab = dest;
	maj();
}
void mystring::ddouble(char c) {
	int i, j;
	int rm = 0;
	char *dest, *src, *origin;
	for (origin = tab; *origin != '\0'; origin++) {
		if (*origin == c) {
			rm++;
		}
	}
	dest = (char*)malloc((strlen(tab) + 1 + rm) * sizeof(char));
	src = dest;
	for (origin = tab; *origin != '\0'; origin++) {
		if (*origin != c) {
			*src = *origin;
			src++;
		}
		else {
			*src = *origin;
			src++;
			*src = *origin;
			src++;
		}
	}
	*src = '\0';
	free(tab);
	tab = dest;
	maj();
}
char* mystring::concat(char *str, char *str2) {
	char *src, *dest;
	src = dest = (char*)malloc((strlen(str) + strlen(str2)) * sizeof(char));
	while ((*src++ = *str++) != '\0');
	src--;
	while ((*src++ = *str2++) != '\0');
	return dest;
}
void mystring::toCaps() {
	for (int i = 0; i < n; i++) {
		if (a <= tab[i] && tab[i] <= z) {
			tab[i] -= 32;
		}
	}
}
mystring& mystring::operator=(const mystring &mstr) {
	if (this != &mstr) {
		if (tab != NULL) {
			delete tab;
		}
		tab = new char[n = mstr.n];
		for (int i = 0; i < n; i++) {
			tab[i] = mstr.tab[i];
		}
	}
	zstat();
	maj();
	return *this;
}
mystring mystring::operator+(const mystring &mstr) {
	char*temp;
	temp = concat(tab, mstr.tab);
	mystring rtn(temp);
	return rtn;
}
mystring mystring::operator-(char c) {
	mystring temp(*this);
	temp.del(c);
	return temp;
}
bool mystring::operator==(const mystring &mstr) {
	if (n != mstr.n) {
		return false;
	}
	else {
		for (int i = 0; i < n; i++) {
			if (tab[i] != mstr.tab[i]) {
				return false;
			}
		}
	}
	return true;
}
bool mystring::operator!=(const mystring &mstr) {
	return !(*this == mstr);
}
ostream& operator<<(ostream &flux, mystring &mstr) {
	for (int i = 0; i < mstr.n; i++)
	{
		flux << mstr.tab[i];
	}
	return flux;
}
void mystring::maj() {
	for (int i = 0; i < n; i++)
	{
		if ('a' <= tolower(tab[i]) && tolower(tab[i]) <= 'z')
		{
			stat[tolower(tab[i]) - 'a']++;
		}
		else
		{
			spe++;
		}
	}
}
void mystring::zstat() {
	for (int i = 0; i < 26; i++) {
		stat[i] = 0;
	}
}

int main()
{
	
	return 0;
}
